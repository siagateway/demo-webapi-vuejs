# SIA API Demo - vuejs

![alt text](./screenshots/sia_vuejs_api_demo1.png "screenshot")

This demo project shows how to perform SIA API calls in order to interract with SIA's data.  
The demo makes use of the [VueJS](https://vuejs.org/) javascript framework and uses the [axios](https://axios-http.com/) javascript library to perform the various RESTful requests to the SIA Web API.

For a complete usage guide of how to use the API, please follow the swagger link below.  
https://help.sia-connect.com/en_US/360001445958-API-reference/360005508478-API-Reference


## Setting up and running the Demo
In the src/components/HelloWorld.vue file of this demo project, change/add the `username` and `password` for SIA and the API URL (if directly connected to SIA via 
`10.20.30.40`, the default should be fine).

```
yarn install
yarn serve
```

Finally, on the host machine open the following link on your browser   
http://localhost:8080/  
or  
http://{IP_ADDRESS}:8080/  
where IP_ADDRESS is the ip address of the host machine.

### General Project setup
```
yarn install
```

### General Compiles and hot-reloads for development
```
yarn serve
```

### General Compiles and minifies for production
```
yarn build
```

### General Lints and fixes files
```
yarn lint
```
